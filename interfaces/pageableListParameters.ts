export interface PageableListParameters {
  page: number;
  perPageAmount: number;
  isDesc: boolean;
  orderBy: string;
}