import { CarModel } from "../enums/carModel";

export interface IRaceMember {
  userId: string;
  userNickname: string;
  car: CarModel;
  typedLength: number;
  CPM: number;
  place: number;
}