export interface SignUpRequest {
  nickname: string;
  email: string;
  password: string;
}