import { CarModel } from "../enums/carModel";

export interface ICar {
  model: CarModel;
  price?: number;
}