export interface IPage<T> {
  entities: Array<T>;
  total: number;
}