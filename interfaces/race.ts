import { IRaceMember } from './raceMember';
import { RaceStatus } from '../enums/raceStatus';
import { IArticle } from './article';

export interface IRace {
  _id: string;
  creatorId: string;
  winnerId: string;
  date: string;
  members: Array<IRaceMember>;
  membersNeeded: number;
  status: RaceStatus;
  article: IArticle;
}