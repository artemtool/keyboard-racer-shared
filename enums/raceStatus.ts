export enum RaceStatus {
  MembersWaiting = 0,
  Started = 1,
  Finished = 2
}